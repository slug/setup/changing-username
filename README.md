# How to Change Username on Ubuntu/Linux Mint (from Command Line)

You use the `usermod` command to change the username (and, other user-related information).
But, it is a bit tricky for various reasons.

If the user account is relatively new (e.g., immediately after the new system installation),
it's much easier just to create a new user with the desired username,
and delete the existing one.


If you have to change the current user's username,
you'll need to have at least one additional user with admin privilege.
Create a new user if you don't have one.

Log in to that account (not to the target account whose username you want to change).
It's best to reboot the system before you do that 
(in case any process is running tied to the target account).

Open a terminal.

    sudo usermod -l _new username_ -d /home/_new username_ -m _old usrname_


